# 梯度下降算法

## 为什么局部下降最快的方向就是梯度的负方向？

### 什么是梯度？
通俗来说，梯度就是表示某一函数在该点处的方向导数沿着该方向取得最大值，即函数在当前位置的导数
```math
\nabla =\frac{df(\theta)}{d\theta}
```

### 梯度下降算法
如果函数$`f(\theta)`$是凸函数，那么就可以使用梯度下降算法进行优化：
```math
\theta = \theta_0-\eta\cdot\nabla f(\theta_0)
```

**那么为什么局部下降最快的方向就是梯度的负方向呢？**

### 一阶泰勒展开式
简单来说，泰勒展开式利用的就是函数的局部线性近似概念，以一阶泰勒展开式为例：
```math
f(\theta)\sim f(\theta_0)+(\theta-\theta_0)\cdot\nabla f(\theta_0)
```

其中，$`\theta-\theta_0`$是微小矢量，它的大小就是步进长度$`\eta`$，为标量，而$`\theta-\theta_0`$用$`v`$表示：
```math
\theta-\theta_0=\eta v
```
其中$`\theta-\theta_0`$是非常小的，太大线性近似就不够准确，一阶泰勒近似也不成立。替换之后，$`f(\theta)`$的表达式为：
```math
f(\theta)\sim f(\theta_0)+\eta v\nabla f(\theta_0)
```
局部梯度下降的目的是希望每次$`\theta`$更新，都能让函数值$`f(\theta)`$变小。也就是说，我们希望$`f(\theta)<f(\theta_0)`$：
```math
f(\theta) - f(\theta_0)\sim\eta v\cdot\nabla f(\theta_0) <0
```
因为$`\eta`$为标量且为正，所以可以忽略：
```math
v\cdot\nabla f(\theta_0) <0
```
**上面的不等式非常重要**，$`v`$和$`\nabla f(\theta_0)`$都是向量，$`\nabla f(\theta_0)`$是当前位置的梯度方向，$`v`$表示下一步前进的单位向量，需要我们求解，有了他，就能根据$`\theta-\theta_0=\eta v`$确定$`\theta`$值了。
想要两个向量的乘积小于零，我们先来看一下两个向量乘积包含哪几种情况：
<img src='../assets/向量乘积.jpg'>

$`A`$和$`B`$均为向量，$`\alpha`$为两个向量之间的夹角。$`A`$和$`B`$的乘积为：
```math
A\cdot B=||A||\cdot||B||\cdot cos(\alpha)
```
$`||A||`$和$`||B||`$均为标量，在$`||A||`$和$`||B||`$确定的情况下，只要$`cos(\alpha)=-1`$，即$`A`$和$`B`$完全反向，就能让$`A`$和$`B`$的向量乘积最小（负最大值）。
顾名思义，当$`v`$与$`\nabla f(\theta_0)`$互为反向，即$`v`$为当前梯度方向的负方向的时候，能让$`v\cdot\nabla f(\theta_0)`$最大程度地小，也就保证了 $`v`$的方向是局部下降最快的方向。
知道 $`v`$是 $`\nabla f(\theta_0)`$的反方向后，可直接得到：
```math 
v = \frac{\nabla f(\theta_0)}{||\nabla f(\theta_0)||}
```
之所以要除以 $`\nabla f(\theta_0)`$的模 $`||\nabla f(\theta_0)`$,是因为 $`v`$是单位向量。
求出最优解 $`v`$之后，带入到 $`\theta=\theta_0=\eta v`$中，得：
```math 
\theta = \theta_0-\eta\frac{\nabla f(\theta_0)}{||\nabla f(\theta_0)||}
```
一般的，因为 $`||\nabla f(\theta_0)||`$是标量，可以斌入到步进因子 $`\eta`$中，即简化为：
```math 
\theta = \theta_0-\eta\nabla f(\theta_0)
```

这样，我们就推导得到了梯度下降算法中 $`\theta`$的更新表达式。


