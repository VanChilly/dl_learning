import numpy as np
import cv2

if __name__ == '__main__':
    h = 800
    w = 600
    # img = np.random.randint(0, 255, size=(h, w, 3))
    img = np.ones((h, w, 3))
    img2 = np.zeros((h // 16, w // 16))
    for j in range(16, h - 16, 16):
        for i in range(16, w - 16, 16):
            cv2.line(img, (i, j), (i + 12, j), (0, 0, 255), 1)
            cv2.line(img, (i, j), (i, j + 6), (0, 0, 255), 1)
            cv2.line(img, (i + 12, j), (i + 12, j + 6), (0, 0, 255), 1)
            cv2.line(img, (i, j + 6), (i + 12, j + 6), (0, 0, 255), 1)
    cv2.imwrite('img.jpg', img)