# 最大似然估计(Maximum Liklihood Estimation, MLE)
假设我们有一个非常复杂的数据分布$`P_{data}(x)`$，但是我们不知道该分布的数学表达式，所以定义一个分布模型$`P_G(x;\theta)`$，该分布由参数$`\theta`$决定。
我们的目标是求得参数$\theta$使得分布$`P_G(x;\theta)`$尽可能的接近$`P_{data}(x)`$.

最大似然估计流程：
1. 从$`P_{data}(x)`$中采集$`m`$个样本$`x_1, x_2, ..., x_m`$
2. 计算样本的似然函数$`L= \prod\limits_{i=1}^m P_G(^i;\theta)`$
3. 求使得似然函数$L$最大的参数$`\theta:\theta^*=\arg\max\limits_\theta\prod\limits_{i=1}^mP_G(x_i;\theta)`$

当来自$`P_{data}(x)$的样本$x_1, x_2, ..., x_m`$在$`P_G(x;\theta)`$分布模型出现的概率越高，也就是$`\prod\limits_{i=1}^mP_G(x^i;\theta)`$越大，$`P_G(x;\theta)和P_{data}(x)越接近。`$

## 对数似然
我们得到目标方程：
```math
L=\prod\limits_{i=1}^mP_G(x^i;\theta)
```
我们需要求出使得$L$最大的时候$\theta$的取值，当似然函数是多个数相乘的时候不容易求解，这时我们对其取对数得到：
```math
L=\sum\limits_{i=1}^mlog(P_G(x^i;\theta))
```

这样一来就变成了求极值的过程，$`\theta`$为自变量。所以只需对$`\theta`$求导，并令导数等于0即可。
## 负对数似然(Negative log-likelihood, NLL)
由于对数似然是对概率分布求对数，概率$`P(x)\in[0,1]`$,取对数后$`P(x)\in[-\infty,0]`$，加负号后$`P(x)\in[0,\infty]`$
```math
l(\theta)=-\sum\limits_{i=1}^mlog(P_G(x^i;\theta))
```

上面的公式非常想交叉熵，只是少了一项$`p(x_i)`$,但是真实标签的概率为1,所以省略了。
我们期望似然估计越大越好，取负后越小越好，所以负对数似然函数可以作为损失函数。值得注意的是，在使用该损失函数的时候并不需要将标签转换成one-hot形式，用所属类别数字表示即可。
在pytorch中，`nn.NLLLoss()`函数虽然叫负对数似然损失函数，**但是该函数内部并没有像公式里那样进行了对数计算**，而是在之前使用了`nn.LogSoftmax()`,所以`nn.NLLLoss()`函数只是做了求和取平均然后再取反的计算，在使用时要配合logsoftmax函数一起使用，或者直接使用交叉熵损失函数。


# 负对数似然损失

```math
NLLLoss = \frac{1}{N}\sum\limits_{i=1}^{N}log(1+e^{-y_iw^Tx_i})
```

# Smooth L1 Loss
```math
Smooth\;L1\;Loss = \left\{ 
    \begin{aligned}
    &0.5x^2, &|x|<1 \\
&|x|-0.5, &x < -1 
    \end{aligned}
\right.
```
smooth l1 loss优势：
让对于在$`[-1,1]`$之间的loss更加鲁棒，即：相比于L2损失函数，其对离群点、异常值（outlier）不敏感，梯度变化相对更小。