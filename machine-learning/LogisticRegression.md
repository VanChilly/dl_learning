# Logistic Regression

逻辑回归又叫对数几率回归，是一种广义的线性回归分类模型。
逻辑回归的原理是用逻辑函数吧线性回归的结果$`(-\infty, \infty)`$映射到$`(0, 1)`$，可能也是叫回归的一种缘由。

## 逻辑函数
```math
\begin{equation}
g(z) = \frac{1}{1+e^{-z}}
\end{equation}
```

### 逻辑函数的导函数
```math
\begin{equation}
g'(z) = g(z) * [1 - g(z)]
\end{equation}
```
逻辑函数是一个连续且仁义阶可导的函数

## 逻辑回归函数
我们令$`y = w^Tx`$,则逻辑回归公式为
```math
\begin{equation}
g(y) = \frac{1}{1+e^{-y}} = \frac{1}{1+e^{-w^tx}}
\end{equation}
```

## 如何求解逻辑回归中的参数$w$?
### 极大似然函数
举个例子，如果小明这次考试考了90分以上，麻麻99%会奖励小明一个手机；而如果没有考到90分以上，麻麻99%不会奖励手机。则现在小明没有得到手机，请问小明考没考到90分以上。

我们的第一反应是小明大概率没有考到90分以上。这种利用已知的样本结果，反推最有可能导致这种结果的参数，就是**极大似然估计**。

已知当前样本x, 那么Y=1的后验概率为
```math
\begin{equation}
p(Y=1|x) = \frac{1}{1+e^{-w^Tx}}
\end{equation}
```
类似的， 当Y=0时的后验概率为
```math
\begin{equation}
p(Y=0|x) = 1 - p(Y=1|x) = \frac{1}{1+e^{w^tx}}
\end{equation}
```
对于一个样本$`(x, y)`$，其标签是y的概率为
```math
\begin{equation}
\begin{aligned}
p(y|x, w) &= (g(x))^y(1-g(x))^{1-y} \\
&s.t. \; y\in\{0, 1\}
\end{aligned}
\end{equation}
```

现在我们有m个客户的观测样本
```math
\begin{equation}
\begin{aligned}
D &= \{(x_1, y_1), (x_2, y_2), ..., (x_m, y_m)\} \\
&s.t. \;x_i\in\mathbb{R}^n
\end{aligned}
\end{equation}
```
则其似然函数方程为
```math
\begin{equation}
\begin{aligned}
L(w) = \prod\limits_{i=1}^mp(y_i|x;w) = \prod\limits_{i=1}^m(g(x_i))^{y_i}(1-g(x_i))^{1-y_i}
\end{aligned}
\end{equation}
```
为了便于求解，我们将上述似然方程转换为负对数似然方程, 也即逻辑回归的损失函数，是关于二分类的损失函数，增加对每个类的损失即变成交叉熵损失函数
```math
\begin{equation}
\begin{aligned}
l(w) &= -\frac{1}{m}ln(L(w)) = -\frac{1}{m}\sum\limits_{i=1}^my_ilog\;g(x_i) + (1-y_i)log\;(1-g(x_i)) 
\end{aligned}
\end{equation}
```
至此，可以采用梯度下降法求解参数$`w`$

[参考](https://cloud.tencent.com/developer/article/1694338)
